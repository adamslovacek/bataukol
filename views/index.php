<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">

    <title>BataUkol</title>
    <meta name="description" content="Ukol">
    <meta name="author" content="Adam Slovacek">

    <link href="/resources/css/bootstrap.css" rel="stylesheet">

</head>

<body>
<div class="container">
    <h1>Hello Bata</h1>

    <div class="batatree">
        {{Tree}}
    </div>
    <div class="messages">

        {{messages}}
    </div>
    <form action="/Tree/addNode" method="post">
        <fieldset>
            <legend>Adding the new nodes</legend>
            Parent:
            <input name="parent">,
            ID:
            <input name="id">
            <input type="submit" value="Add node">
        </fieldset>
    </form>
    <form action="/Tree/deleteNode" method="post">
        <fieldset>
            <legend>Deleting nodes</legend>
            ID:
            <input name="id">
            <input type="submit" value="Delete node">
        </fieldset>
    </form>
    <form action="/Tree/emptyTree" method="post">
        <fieldset>
            <legend>Delete all nodes</legend>
            <input type="submit" value="EmmptyTree node">
        </fieldset>
    </form>
</div>
<script src="/resources/js/bootstrap.js" type="text/javascript"></script>
</body>
</html>