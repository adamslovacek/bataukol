<?php
namespace InstanceBataUkol;

class InstanceSetup
{
    /**
     * @var string[]
     */
    private $configuration=[
        'DBhost'=>'localhost',
        'DBusername'=>'bata',
        'DBdbname'=>'bata',
        'DBpassword'=>'atab',
        'DBport'=>'3306',
        'DBcollation'=>'utf8',
        'bootstrap'=>'library/php/BataUkol/bootstrap.php',
        'controllers'=>'controllers',
        'phpLibraryPath'=>'library/php',
        'display_errors'=>1,
        'display_startup_errors'=>1,
        'error_reporting'=>E_ALL,
    ];

    /**
     * @param string $id -identifier
     * @return string - calue
     */
    public function get(string $id):string{
        return $this->configuration[$id];
    }

    public function requireBootstrap()
    {
        require_once($this->get('bootstrap'));
    }

    function __construct()
    {
        $this->setErrorReporting();
    }

    protected function setErrorReporting(){
        ini_set('display_errors', $this->get('display_errors'));
        ini_set('display_startup_errors',  $this->get('display_startup_errors'));
        error_reporting($this->get('error_reporting'));
    }
}