<?php
namespace controlers;

use BataUkol\Api\BataNodeException;
use BataUkol\Api\ControllerException;
use BataUkol\Parameters;
use models\BataNode;
use BataUkol\Api\Controller;
use models\BataNodeContent;
use models\BataTree;
use Womfy\Template;

/**
 * Created by PhpStorm.
 * User: adamse
 * Date: 2.11.16
 * Time: 20:28
 */
class Tree extends Controller
{

    protected $tempplatePathBataNode = "batanode";

    protected $tempplatePathBataTree = "batatree";

    protected $tempplatePathBataNodeGroup = "nodegroup";


    public function index(){

        $bataTree=new BataTree();

        $templateNode = new Template($this->tempplatePathBataNode, null, "template");
        $templateNodeGroup = new Template($this->tempplatePathBataNodeGroup, null, "template");
        $templateTree = new Template($this->tempplatePathBataTree, null, "template");

        $maxDepth = $bataTree->getTreeDepth();

        for ($i = 0; $i < $maxDepth; $i++) {
            $templateNodeGroup->clear();
            $templateNodeGroup->add('depth', $i + 1);

            $levelNodes = $bataTree->getLevelNodes($i);
            $splitted = $bataTree->splitLevelNodesByParent($levelNodes);

            foreach ($splitted as $parentId => $nodeListLevel) {
                $templateNode->clear();
                $templateNode->add('parentid', $parentId);
                foreach ($nodeListLevel as $node) {
                    $templateNode->add('nodes', $node->getNodeContent()->getContent().', ');
                }
                $templateNodeGroup->add('nodes', $templateNode->getString());
            }
            $templateTree->add('Tree', $templateNodeGroup->getString());
        }

        $template=new Template("index",null,"template");
        $template->add('Tree', $templateTree->getString());
        $MessageTemplate=new Template('message',null,'template');
        foreach(ControllerException::getMessages() as $Message){
            $MessageTemplate->clear();
           $MessageTemplate->add('message',$Message);

            $template->add('messages',$MessageTemplate->getString());

        }
        return $template->getString();
    }

    public function addNode(){

        try {
            if (!Parameters::getParameter('parent') || !Parameters::getParameter('id')) {
                if(Parameters::getParameter('parent')==='' && Parameters::getParameter('id')){
                    $node=new BataNode();
                    $node->loadRoot();

                    if($node->getNodeId()){
                        throw new ControllerException("There might be only one root node");
                    }else{
                        $node->setParentId(0);
                        $content=new BataNodeContent();
                        $content->setContent(Parameters::getParameter('id'));
                        $node->setNodeContent($content);
                        $node->save();
                    }
                }else{

                    throw new ControllerException("Parent id or id not set");
                }

            } else {
                $node = new BataNode();
                $parent=$node->getPatentIdByContent(Parameters::getParameter('parent'));
                if(!$parent){
                    throw new ControllerException("Parent not found");
                }else{

                    $node->setParentId($parent);
                    $nodeContent = new BataNodeContent();
                    $nodeContent->setContent(Parameters::getParameter('id'));
                    $node->setNodeContent($nodeContent);
                    $node->save();
                }
            }

        }catch(ControllerException $e){

        }
        catch (BataNodeException $e){

        }
        return $this->index();
    }

    public function deleteNode(){
        $node = new BataNode();
        try {
        if (!Parameters::getParameter('id')) {
            throw new ControllerException("Id of node not set");
        }else {

                $id = $node->getPatentIdByContent(Parameters::getParameter('id'));
                if (!$id) {
                    throw new ControllerException("Node not found");
                } else {
                    $node->load($id);
                    $node->delete();
                }

        }
        } catch (ControllerException $e) {

        }
        return $this->index();
    }

    public function emptyTree(){
        $node=new BataNode();
        $node->loadRoot();
        $node->delete();
        return $this->index();
    }
}