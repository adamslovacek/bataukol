<?php
namespace Womfy;
/*
 * parses the string or file and makes template object
 */

class Template {

    private $settings;
    private $source;
    private $types = array("string", "file", "db", "template");
    private $marksContent = array();
    private $marks = array();
    private $disabledSpecialMarks = array();

    /**
     * Initialize Template object. it just check if the source if OK
     *
     * @param string mixedSource the data to take string for template from
     * @param null|settings the settings object
     * @param string type of template: file, string or db
     *
     */
    public function __construct($mixedSource, $settings, $type="file") {
        if (!in_array($type, $this->types)) {
            trigger_error('Unknown template type: ' . $type, E_USER_ERROR);
        }
        $this->settings['template'] = $this;
        $this->settings = $settings;
        $this->source = $this->getStringForTemplate($mixedSource, $type);
    }

    public function setSettings($settings) {
        $this->settings = $settings;
    }

    public function toString() {
        return $this->toString();
    }

    /**
     * Serves to add single mark into template. It does not puts the content into template directly
     * but stores in field instead.
     *
     * All the marks are filled in parse() method
     *
     * @param string markName the name of the mark (type 0,1,2)
     * @param string value the desired conntent
     */
    public function add($markName, $value) {

        if (!isset($this->marksContent[$markName]))
            $this->marksContent[$markName] = $value;
        else
            $this->marksContent[$markName].=$value;
    }

    /**
     * Serves to add single mark into template. It does not puts the content into template directly
     * but stores in field instead.
     *
     * All the marks are filled in parse() method
     *
     * @param markName the name of the mark (type 0,1,2)
     * @param value the desired conntent
     */
    public function addPre($markName, $value) {

        if (!isset($this->marksContent[$markName]))
            $this->marksContent[$markName] = $value;
        else
            $this->marksContent[$markName] = $this->marksContent[$markName] . $value;
    }

    /**
     * replaces content of some mark wihn another. So there is no need to
     * call clean(mark) add(mark, content)
     *
     * @param markName the name of the mark (type 0,1,2,3)
     * @param value the desired conntent
     * ..
     */
    public function replace($markName, $newValue) {
        $this->marksContent[$markName] = $newValue;
    }

    /**
     * cleans single mark all whole template
     *
     * @param markName the name of the mark
     */
    public function clear($markName=null) {
        if ($markName == null) {
            $this->marksContent = array();
        } else {
            unset($this->marksContent[$markName]);
        }
    }

    /**
     * serves to get the template output
     *
     * @return the result strign representing the filled template
     */
    public function getString() {
        return $this->parse(false);
    }

    /**
     * serves to obrain all the marks in template. Note tha it could be called
     * after parse method, while it parses the marks
     *
     * @return all the Mark names in template
     */
    public function getAllMarks() {
        return $this->marks;
    }

    /**
     * serves to obrain all the marks in template and content. Note tha it could be called
     * after parse method, while it parses the marks
     *
     * @return all the Mark names in template
     */
    public function getAllMarksContent() {
        return $this->marksContent;
    }

    /**
     * Only type 0,1,2,3 are editable by user
     *
     * @param optional max level of editable marks. default 3
     * @return the user editable marks
     */
    public function getUserEditableMarksAndTypes($maxLevel=null) {
        if ($maxLevel === null)
            return array_merge($this->marks[0], $this->marks[1], $this->marks[2], $this->marks[3]);
        $tmlArr = array();
        for ($i = 0; $i <= $maxLevel; $i++) {
            if (isset($this->marks[$i]))
                $tmlArr = array_merge($this->marks[$i]);
        }
        return $tmlArr;
    }

    /**
     * Returns the user editable marks in the format more suitable for
     * for example article module
     *
     * @return the user editable marks as array where the key is the mark name
     * and the content of each element is array, where:
     * 0=>mark type
     * 1=>-1 (indicates that it is not known if the mark is also savet in database )
     * 2=>1 (indicates that it is the template mark)
     * 3=>"" (mark content)
     *
     * Note that the mark the hightest mark type is priorized. No duplicates generated
     */
    public function getMarksArticles($maxType=null) {
        if ($maxType === null)
            $maxType = 3;
        $tmp = array();
        for ($i = 0; $i <= $maxType; $i++) {
            if (isset($this->marks[$i]))
                foreach ($this->marks[$i] as $markname) {
                    $tmp[$markname] = array($i, 0, 1, array());
                }
        }
        return $tmp;
    }

    /**
     * parses and may fill the template
     *
     * @param onlyParse_DoNotFillUp if true the Marks are not replaced with
     * their desired content but leaved blank insted. if false the outpur is generated
     *
     *
     * @return filled or not filled template
     */
    public function parse($onlyParse_DoNotFillUp=false) {


        $source = $this->source;
        $lenght = strlen($this->source);
        $state = 0;
        $tmpMark = "";
        $i = 0;
        $resultString = "";
        $tempSequence = "";
        while ($i < strlen($source)) {
            $char = $source{$i};
            switch ($state) {
                case 0:
                    if ($char == '{') {
                        $state = 1;
                        $tempSequence.='{';
                    } else if ($char == '\\') {
                        $state = 5;
                        $tempSequence.='\\';
                    } else {
                        $resultString.=$char;
                        $tempSequence = "";
                    }
                    $i++;
                    break;
                case 1:
                    if ($char == '{') {
                        $state = 2;
                        $tempSequence = "";
                    } else if ($char == '\\') {
                        $state = 5;
                        $tempSequence.='\\';
                    } else {
                        $resultString.='{' . $char;
                        $tempSequence = "";
                        $state = 0;
                    }
                    $i++;
                    break;
                case 2:
                    if ($char == '}') {
                        $state = 3;
                    } else
                        if ($char == '\\') {
                            $state = 7;
                        }
                    $tmpMark.=$char;
                    $i++;
                    break;
                case 3:
                    if ($char == '}') {
                        if (!$onlyParse_DoNotFillUp)
                            $resultString.=$this->tryToParseSingleKey(substr($tmpMark, 0, -1));
                        $tmpMark = "";
                        $state = 4;
                    } else if ($char == '\\') {
                        $state = 7;
                        $tmpMark.=$char;
                    } else {
                        $tmpMark.=$char;
                        $state = 2;
                    }
                    $i++;
                    break;
                case 4:
                    if ($char == '{') {
                        $state = 1;
                    } else if ($char == '\\') {
                        $tempSequence = '\\';
                        $state = 5;
                    } else {
                        $state = 0;
                        $resultString.=$char;
                    }
                    $i++;
                    break;
                case 5:
                    if ($char == '{') {
                        $state = 6;
                        $tempSequence.='{';
                        $i++;
                    } else {
                        $resultString.='\\';
                        $state = 0;
                    }
                    break;
                case 6:
                    if ($char == '{') {
                        $state = 0;
                        $resultString.='{{';
                        $i++;
                    } else {
                        $resultString.='\{';
                        $state = 0;
                    }
                    break;
                case 7:
                    if ($char == '}') {
                        $state = 8;

                        $i++;
                    } else {
                        $state = 2;
                    }

                    break;
                case 8:
                    if ($char == '}') {
                        $state = 2;
                        $tmpMark.=$char;
                        $tmpMark.=$char;
                        $i++;
                    } else {
                        $state = 2;
                    }
                    break;
            }
        }
//return 'ddd';

        return $resultString;
    }

    /**
     * Serves to parse a single mark content ad determine the type of it
     *
     * @param string the mark specification
     * @return the desired mark content
     *
     */
    private function tryToParseSingleKey($string) {
        $parts = explode(",", $string);

        if (!isset($parts[1])) {
            $type = WomfyInternalSettings::$defaultMarkType;
        } else {
            $type = $parts[1];
        }

        $this->marks[$type][] = $parts[0];
        if ($type >= -1) {
            return isset($this->marksContent[$parts[0]]) ? $this->marksContent[$parts[0]] : '';
        } else if ($type == -1) {

        } else if ($type == -2) {
            if (isset($this->disabledSpecialMarks[$parts[0]])) {

                return $this->disabledSpecialMarks[$parts[0]];
            }else
                return Page::callFunction($parts[0], $this->settings,$this);
        }else {
            trigger_error('Unknown mark type in Template: ' . $type, E_USER_ERROR);
        }
    }

    /**
     * if some marks specified the calling syntax is not triggered
     *
     */
    public function replaceSpecialMark($markCallingSyntax, $content) {
//echo $markCallingSyntax.'vv'.$content;
        $this->disabledSpecialMarks[$markCallingSyntax] = $content;
    }

    public function pushMysqlRow($mysqlRow) {
        foreach ($mysqlRow as $markname => $markValue) {
            $this->add($markname, $markValue);
        }
    }
    public function pushMysqlRowReplace($mysqlRow) {
        foreach ($mysqlRow as $markname => $markValue) {
            $this->replace($markname, $markValue);
        }
    }
    public function getSpecialsToReplace() {
        return $this->disabledSpecialMarks;
    }

    /**
     * Serves to obrain a raw template string from various sourced
     *
     * @param mixedSource some data representing source
     * @return a string representing the template
     */
    private function getStringForTemplate($mixedSource, $type) {

        switch ($type) {
            case "db" :
                trigger_error('Template from database: not supported yet', E_USER_ERROR);
                break;
            case "template" :
                return file_get_contents(WomfyInternalSettings::$templateDir.'/'.$mixedSource.".".WomfyInternalSettings::$moduleTemplateExtension);
                break;
            case "string" :
            case "raw" :
                return $mixedSource;
            case "file" :
                return file_get_contents($mixedSource );
                break;

            default :
                break;
        }
    }

}

?>