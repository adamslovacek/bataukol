<?php

namespace Womfy;


/*
 * These are a very secret or base setup values of womfy system
 */

class WomfyInternalSettings {

    //recaptcha fields
    public static $recaptchaDomain="light.womfy.com";
    /**
     * if the mark type is not specified, this value is used instead
     */
    public static $defaultMarkType=0;
    /*
     * The file template path
     *
     * */

    public static $templateDir = "views";
    /**
     * says how to recognize the module temlate from another one
     */
    public static $moduleTemplateExtension = "php";
    /**
     * says how to recognize the module temlate from another one
     */
    public static $moduleTemplatePrefix = "module_template_";
    /**
     * The minimal interval between each the login try in seconds
     */
    public static $minimalLoginInterval = 5;
    /**
     * the prefix for Module classes... so there is no need to write
     * "Module_Articles", but "Articles" only
     */
    public static $moduleClassPrefix = "Modules_";

    /**
     * The number of rounds script should recycling hashing the password
     *
     */
    public static $modulesData = 'data/modules';

    public static $defaultController="Tree";

}

?>