<?php

namespace BataUkol;


use BataUkol\Api\NodeList;
use models\BataNode;

class BataNodelist extends NodeList {
    /**
     * load nodes from database by specified query and initializes itsef
     *
     * @param string $string
     */
    public function loadByQuery(string $string) {

        $statement=Database::execute($string);

        while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {

            $node=new BataNode();
            $node->load($row['id']);
            $this->addNode($node);

        }
        $this->rewind();
    }
}