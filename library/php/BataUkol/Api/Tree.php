<?php
namespace BataUkol\Api;

interface Tree
{


    /**
     * remove node by id
     *
     * @param string $id
     */
    public function removeNode(string $id);

    /**
     * get node by id
     * @param string $id
     * @return Node
     */
    public function getNode(string $id):Node;

    /**
     * get parent node of some other node
     *
     * @return Node
     */
    public function getParentNode(Node $node):Node;

    /**
     * get all nodes under specidied node
     *
     * @return NodeList
     */
    public function getChildNodes(Node $node):NodeList;

    /**
     *
     * get all nodes from same parent
     * @return NodeList
     */
    public function getSyblingNodes(Node $node):NodeList;

    public function getAllNodes(Node $fromNode=null):NodeList;

    public function nodeExists(string $id):bool;

    public function getTreeDepth():int;
}