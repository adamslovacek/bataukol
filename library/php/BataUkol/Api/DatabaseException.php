<?php

namespace BataUkol\Api;


class DatabaseException extends \Exception
{

    /**
     * DatabaseException constructor.
     */
    public function __construct($message)
    {
        parent::__construct($message);
    }
}