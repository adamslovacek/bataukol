<?php

namespace BataUkol\Api;


abstract class NodeList implements \Iterator
{
    /**
     * @var int
     */
    private $position = 0;
    /**
     * @var Node[]
     */
    private $array = [];

    public function __construct() {
        $this->position = 0;
    }

    function rewind() {

        $this->position = 0;
    }

    function current():Node {

        return $this->array[$this->position];
    }

    function key():int {

        return $this->position;
    }

    function next() {

        ++$this->position;
    }

    function valid():bool {

        return isset($this->array[$this->position]);
    }

    public function addNode(Node $node){
        $this->array[]=$node;

    }

    abstract public function loadByQuery(string $string);


 }