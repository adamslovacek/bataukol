<?php

namespace BataUkol\Api;


interface NodeContent {
    /**
     * the name of the class
     * @return string
     */
    public function getClass(): string;

    /**
     * the content as a instance of used class
     * @return string|mixed
     */
    public function getContent();

    public function setContent($GenericContent);
}