<?php

namespace BataUkol\Api;


interface Node
{
    public function getNodeId():string;
    public function getNodeContent():NodeContent;
    public function getNodePosition():int;
    public function getParentId():string;

    public function setNodeContent(NodeContent $content);
    public function setParentId(int $parent);
    public function setNodeParentByNode(Node $node);


}