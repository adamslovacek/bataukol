<?php

namespace BataUkol\Api;

/**
 * storeable database object
 * Interface DatabaseObject
 * @package BataUkol\Api
 */
interface DatabaseObject {

    /**
     * stores into th db
     * @return mixed
     */
    public function save();

    /**
     * loads by id
     * @param string $id
     * @return mixed
     */
    public function load(string $id);
}