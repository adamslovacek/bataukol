<?php
namespace BataUkol\Api;

abstract class Controller
{
    /**
     * runs the proppes part of controller
     *
     * @param string $call
     * @return mixed
     */
    public function run(string $call){
        return $this->$call();
    }


}