<?php

namespace BataUkol\Api;


interface NestedSetNode extends \BataUkol\Api\Node {
    public function getLeftId();

    public function getRightId();


}