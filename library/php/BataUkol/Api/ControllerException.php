<?php

namespace BataUkol\Api;


class ControllerException extends \Exception
{


    /**
     * @var string[]
     */
protected static $messages=array();

    /**
     * stores messafes
     * ControllerException constructor.
     * @param string $message
     */
    public function __construct( $message)
    {
        self::$messages[]=$message;
        parent::__construct($message);
    }

    /**
     * obrtain messages
     * @return \string[]
     */

    public static function getMessages(){
        return self::$messages;
    }
}