<?php
namespace BataUkol;

use InstanceBataUkol\InstanceSetup;

class Autoload {

    /**
     * @var bool
     */
    private static $registered=false;

    /**
     * @var InstanceSetup
     */
    private static $instanceSetup=null;

    public static function registerAutoload(){
        if(!self::$registered){
            spl_autoload_register(array('\BataUkol\Autoload','load'),true);
            self::$registered=true;
        }
        function __autoload($sClass){}
    }

    public static function load(string $sClass){
        if(static::$instanceSetup===null){
            static::$instanceSetup=new InstanceSetup();
        }

        $sFile=static::$instanceSetup->get('phpLibraryPath').DIRECTORY_SEPARATOR.str_replace("\\",DIRECTORY_SEPARATOR,$sClass) . ".php";

        if(file_exists($sFile)){
            include_once $sFile;
        }

        if(!class_exists($sClass) && !interface_exists($sClass)){
            require_once self::LoadFromRoot($sClass);
        }

    }

    public static function LoadFromRoot($sClass){
        return  str_replace("\\",DIRECTORY_SEPARATOR,$sClass) . ".php";
    }

}