<?php
namespace BataUkol;

use Womfy\Template;
use Womfy\WomfyInternalSettings;

/**
 * very simple application
 * Class Application
 * @package BataUkol
 */
class Application
{
    private $database;
    private $content;

    /**
     * content is the generated code json/html(...)
     * @return string
     */
    public function toString():string
    {
        return $this->content;

    }

    /**
     * runs all lofic include content getting
     *
     * @param string $query
     */
    public function run(string $query)
    {
        $parts=explode("/",$query);
        $controler=strlen($parts[0])<1?WomfyInternalSettings::$defaultController:$parts[0];
        $funtion=$parts[1]??"index";
        $controler="controlers\\".$controler;

        $controler=new $controler();
        $this->content=$controler->run($funtion);
    }

    /**
     * initializes applicatoon (database handler)
     * @param $instanceSetup
     */
    public function init( $instanceSetup){
        $this->database=new Database($instanceSetup);

    }
}