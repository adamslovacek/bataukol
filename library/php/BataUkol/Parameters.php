<?php

namespace BataUkol;

/**
 * Very simple parameter getter
 * Class Parameters
 * @package BataUkol
 */
class Parameters
{
    /**
     * serves to get the parameter value from different sources
     *
     * @param string $parameter - the parameter name
     * @return null|string the parameter content
     */
    public static function getParameter(string $parameter)
    {
        return $_POST[$parameter]??($_GET[$parameter]??null);
    }

}