<?php

namespace BataUkol;


use BataUkol\Api\DatabaseException;
use InstanceBataUkol\InstanceSetup;


/**
 *
 * Database handler
 *
 * Class Database
 * @package BataUkol
 */
class Database
{
    /**
     * @var Database
     */
    protected static $database = null;

    /**
     * @var \PDO
     */
    protected $PDO = null;

    /**
     * @var InstanceSetup|null
     */
    protected $instanceSetup = null;

    /**
     * Construct initialize instance
     *
     * Database constructor.
     * @param InstanceSetup $setup
     */
    public function __construct(InstanceSetup $setup)
    {
        if (self::$database === null) {
            self::$database = $this;
            $this->instanceSetup = $setup;
            $this->PDO = new \PDO("mysql:host={$setup->get("DBhost")};dbname={$setup->get("DBdbname")}", $setup->get("DBusername"), $setup->get("DBpassword"));
            $this->execute("SET NAMES '{$setup->get("DBcollation")}'");
        }

    }

    /**
     * executes query and retusnd result value
     * @param string $query
     * @return \PDOStatement
     * @throws DatabaseException
     */
    public static function execute(string $query):\PDOStatement
    {
        $result = self::$database->PDO->query($query);
        if (!$result) {
            throw new DatabaseException("Invalid query: " . $query);
        }
        return $result;

    }

    /**
     * alias for PDO quote method
     * @param string $string
     * @return string
     */
    public static function quote(string $string):string
    {
        return self::$database->PDO->quote($string);
    }

    /**
     * serves to get current pdo object
     * @return \PDO
     */
    public static function getDB()
    {
        return self::$database->PDO;
    }

}