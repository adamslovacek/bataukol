CREATE TABLE `nodes` (
  `id`       INT(11)     NOT NULL AUTO_INCREMENT,
  `parentid` INT(11)     NOT NULL,
  `content`  VARCHAR(32) NOT NULL,
  `leftid`   INT(11)     NOT NULL,
  `rightid`  INT(11)     NOT NULL,
  `datetime` TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 666
  DEFAULT CHARSET = utf8