<?php

namespace models;


use BataUkol\Api\BataNodeException;
use BataUkol\Api\DatabaseException;
use BataUkol\Api\DatabaseObject;
use BataUkol\Api\NestedSetNode;
use BataUkol\Api\Node;
use BataUkol\Api\NodeContent;
use BataUkol\Database;

class BataNode implements NestedSetNode,DatabaseObject
{
    /**
     * @var string[]
     */
    protected $data=[];

    /**
     * @var BataNodeContent
     */
    protected $nodeContent=null;

    /**
     * load node by UID
     *
     * @param string $id
     */
    public function load(string $id){
        $statement=Database::execute("select * from nodes where id=".Database::quote($id));
        $this->data=$statement->fetch(\PDO::FETCH_ASSOC);
        $content=new BataNodeContent();
        $content->setContent($this->data['content']);
        $this->setNodeContent($content);
    }

    /**
     * deletes current node from db
     */
    public function delete(){


        $rightid=Database::quote($this->getRightId());
        $width=Database::quote($this->getRightId()-$this->getLeftId()+1);

        Database::getDB()->beginTransaction();
        Database::execute("delete from nodes where id=".Database::quote($this->getNodeId()) ." or nodes.leftid between ".Database::quote($this->getLeftId()) ."  and ".Database::quote($this->getRightId()) ." ");
        Database::execute("UPDATE nodes SET rightid = rightid - $width WHERE rightid >$rightid");
        Database::execute("UPDATE nodes SET leftid = leftid - $width WHERE leftid > $rightid");
        Database::getDB()->commit();


    }

    /**
     * serrves to get node id
     * @return string
     */
    public function getNodeId():string {
        return $this->data['id']??"";
    }

    /**
     * serves to get node content
     *
     * @return string
     */
    public function getNodeContent():NodeContent {
      return  $this->nodeContent;
    }

    public function getNodePosition():int {
        return  $this->data['position'];
    }


    public function setParentId(int $parent) {
        $this->data['parentid']=$parent;
    }

    public function setNodeParentByNode(Node $node) {
        $this->data['parentid']=$node->getParentId();
    }

    public function getLeftId() {
        return $this->data['leftid']??"";
    }

    public function getRightId() {
        return $this->data['rightid']??"";
    }

    public function setNodeContent(NodeContent $content) {
        $this->nodeContent=$content;
    }

    public function getParentId():string {
        return $this->data['parentid']??"";
    }

    /**
     * saves node and throws exception if something is not that good
     *
     * @throws BataNodeException
     */
    public function save() {

        $id=$this->data['id']??"";
        if($id===""){
            $id=$this->data['id']??"";
            $statement=Database::execute("select count(*) as cnt from nodes where content=".Database::quote($this->getNodeContent()->getContent()));
            $data=$statement->fetch(\PDO::FETCH_ASSOC);
            if($data['cnt']<1){
                if($this->getParentId()!=""){
                    try {

                        $statementRight = Database::execute("SELECT rightid AS rightid FROM nodes WHERE id=" . Database::quote($this->getParentId()));
                        $right = $statementRight->fetch(\PDO::FETCH_ASSOC)['rightid']??1;
                        Database::getDB()->beginTransaction();
                        Database::execute("update nodes set rightid=rightid+2 where rightid>=".Database::quote($right));
                        Database::execute("update nodes set leftid=leftid+2 where leftid>=".Database::quote($right));
                        Database::execute("insert into nodes (parentid,rightid,leftid,content) values (".Database::quote($this->getParentId()).",".Database::quote($right+1).",".Database::quote($right).",".Database::quote($this->getNodeContent()->getContent()).")");
                        Database::getDB()->commit();
                    }catch (DatabaseException $e){
                        echo $e->getMessage();
                    }
                }else{
                    throw new BataNodeException("Unknown parent ID");
                }
            }else{
                throw new BataNodeException("Duplication node ID");
            }
        }else{
            throw new BataNodeException("Update Not Implemented");
        }
    }

    public function loadRoot()
    {
        $statement=Database::execute("select * from nodes where parentid=".Database::quote("0"));
        $this->data=$statement->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * get id by content
     *
     * @param string $contentString
     * @return mixed
     */

    public function getPatentIdByContent(string $contentString)
    {
        $statement = Database::execute("SELECT id FROM nodes WHERE content=" . Database::quote($contentString));
        return $statement->fetch(\PDO::FETCH_ASSOC)['id'];
    }
}