<?php

namespace models;




class BataNodeContent implements \BataUkol\Api\NodeContent,\Serializable  {

    /**
     * @var string
     */
    protected $content="";

    /**
     * the name of the class
     *
     * @return string
     */
    public function getClass(): string {
        return "string";
    }

    /**
     * the content as a instance of used class
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * String representation of object
     *
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize() {
        return $this->content;
    }

    /**
     * Constructs the object
     *
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized) {
         $this->content=$serialized;
    }

    public function setContent($GenericContent) {
        $this->content=$GenericContent;
    }
}