<?php

namespace models;

use BataUkol\Api\Node;
use BataUkol\Api\NodeList;
use BataUkol\Api\Tree;
use BataUkol\BataNodelist;
use BataUkol\Database;

class BataTree implements Tree
{


    /**
     * remove node by id
     *
     * @param string $id
     */
    public function removeNode(string $id)
    {
        $node=new BataNode();
        $node->load($id);
        $node->delete();
    }

    /**
     * get node by id
     * @param string $id
     * @return Node
     */
    public function getNode(string $id):Node
    {
        $node=new BataNode();
        $node->load($id);
        return $node;
    }

    /**
     * get parent node of some other node
     *
     * @return Node
     */
    public function getParentNode(Node $node):Node
    {
        $node=new BataNode();
        $node->load($node->getParentId());
        return $node;
    }

    /**
     * Metho not optimal. only illistrates enclosed object approach
     *
     * @return NodeList
     */
    public function getChildNodes(Node $node):NodeList
    {
        $nodelist=new BataNodelist();
        $nodelist->loadByQuery("select id from nodes where parentid=".Database::quote($node->getNodeId()));
        return $nodelist;
    }

    /**
     *
     * get all nodes from same parent
     * @return NodeList
     */
    public function getSyblingNodes(Node $node):NodeList
    {
        $nodelist=new BataNodelist();
        $nodelist->loadByQuery("select id from nodes where parentid=".Database::quote($node->getNodeId()));
        return $nodelist;
    }


    public function getAllNodes(Node $parentNode=null) :NodeList{
        if($parentNode===null){
            $parentNode=new BataNode();
            $parentNode->loadRoot();
        }
        $nodelist=new BataNodelist();
        $ledtid=Database::quote($parentNode->getLeftId());
        $rightId=Database::quote($parentNode->getRightId());

        $selectString="SELECT * FROM nodes WHERE leftid BETWEEN $ledtid AND $rightId  ORDER BY leftid;";
        $nodelist->loadByQuery($selectString);
        return $nodelist;
    }

    public function nodeExists(string $id):bool {
        $node=new BataNode();
        $node->load($id);
        return $node->getNodeId()==$id;

    }

    public function getLevelNodes($iLevel):NodeList
    {

        $select = "SELECT node.*, (COUNT(parent.id) - 1) AS depth FROM nodes AS node, nodes AS parent WHERE node.leftid BETWEEN parent.leftid AND parent.rightid GROUP BY node.id HAVING depth=" . (intval($iLevel)) . " ORDER BY node.parentid;";
        $nodelist = new  BataNodelist();
        $nodelist->loadByQuery($select);
        return $nodelist;
    }

    /**
     * @param NodeList $list
     * @return NodeList[]
     */
    public function splitLevelNodesByParent(NodeList $list)
    {
        $LastParent = null;
        $nodeLists = array();
        $currentNodelist = null;
        foreach ($list as $node) {
            if ($LastParent != $node->getParentId()) {
                $currentNodelist = new BataNodelist();
                $nodeParent = new BataNode();
                $nodeParent->load($node->getParentId());
                $nodeLists[$nodeParent->getNodeContent()->getContent()] = $currentNodelist;
                $LastParent = $node->getParentId();
            }
            $currentNodelist->addNode($node);
        }
        return $nodeLists;
    }

    public function getTreeDepth():int
    {

        $select = "SELECT  (COUNT(parent.id) - 1) AS depth FROM nodes AS node, nodes AS parent WHERE node.leftid BETWEEN parent.leftid AND parent.rightid GROUP BY node.id  ORDER BY depth DESC, node.datetime ASC  LIMIT 1;";
        return 1 + Database::execute($select)->fetch(\PDO::FETCH_ASSOC)['depth'];

    }
}