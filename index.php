<?php
if (PHP_VERSION_ID < 70000) {
    trigger_error("PHP 7 required");
    echo "PHP 7 required";
    exit;
}
require_once("InstanceSetup.php");
$instanceSetup=new \InstanceBataUkol\InstanceSetup(__DIR__);
$instanceSetup->requireBootstrap();

$application=new \BataUkol\Application();
$application->init($instanceSetup);
$application->run($_SERVER['QUERY_STRING']);
echo $application->toString();